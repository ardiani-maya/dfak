---
layout: page
title: PGP
author: mfc
language: es
summary: métodos de contacto
date: 2018-09
permalink: /es/contact-methods/pgp.md
parent: /es/
published: true
---

PGP (En Inglés Pretty Good Privacy) y su equivalente de código abierto, GPG (En Inglés Gnu Privacy Guard), te permite cifrar el contenido de los correos electrónicos para proteger tu mensaje y evitar que lo vea tu proveedor de correo electrónico o cualquier otro que pueda tener acceso a tus correos. Sin embargo, el hecho de que haya enviado un mensaje a la organización receptora puede ser accesible por los gobiernos u organismos de seguridad. Para evitar esto, puedes crear una dirección de correo electrónico alternativo no asociada con tu identidad.

Recursos: [Documentación para la comunidad de la línea de ayuda en seguridad digital de Access Now: correo electrónico seguro](https://communitydocs.accessnow.org/253-Secure_Email_Recommendations.html)

[Freedom of the Press Foundation: Cifrar el correo electrónico con Mailvelope: una guía para principiantes](https://freedom.press/training/encrypting-email-mailvelope-guide/) 

[Herramientas de privacidad: proveedores de correo electrónico privados](https://www.privacytools.io/providers/email/)
